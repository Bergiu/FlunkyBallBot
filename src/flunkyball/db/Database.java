package flunkyball.db;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Methods that are needed to save and load data
 *
 * @author bergiu
 *
 */
public interface Database {

	/*
	 * removes all previous items and saves all new items to the specified table
	 */
	public void saveItems(String dbTableName, ArrayList<HashMap<String, String>> items);

	/* returns all Items needed for the controller from the specified table */
	public ArrayList<HashMap<String, String>> loadItems(String dbTableName);

	/* updates multiple items in the database */
	public void updateItems(String dbTableName, ArrayList<HashMap<String, String>> items);

	public void updateItem(String dbTableName, HashMap<String, String> item);

	/**
	 * inserts items to the end of a table
	 *
	 * @param dbTableName
	 * @param items
	 * @throws Error
	 *             if id is taken
	 */
	public void insertItems(String dbTableName, ArrayList<HashMap<String, String>> items);

	public void insertItem(String dbTableName, HashMap<String, String> item);

	/* returns the id for the next item */
	public int getNextId(String dbTableName);

}
