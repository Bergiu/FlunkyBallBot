package flunkyball.db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * We don't know how to access a database in Java, so we will save the data in
 * files
 *
 * @author bergiu
 *
 */
public class FileConnector implements Database {
	// TODO: vllt irgendwann
	// private String dataDirectory;

	@Override
	public void saveItems(String dbTableName, ArrayList<HashMap<String, String>> items) {
		// lines to save
		String lines = "";

		// all keys from the items
		ArrayList<String> keys = this.getKeysFromHashMaps(items);

		// alle keys in die erste Zeile schreiben
		String first_line = "";
		for (int i = 0; i < keys.size(); i++) {
			if (i == 0) {
				first_line += keys.get(i);
			} else {
				first_line += ";" + keys.get(i);
			}
		}
		lines = first_line;

		// alle zeilen durchgehen und durch den jeweiligen key die value holen
		for (HashMap<String, String> hm : items) {
			String line = "";
			for (int i = 0; i < keys.size(); i++) {
				String value = hm.get(keys.get(i));
				if (value == null) {
					value = "";
				}
				value = this.normalizeValue(value);
				if (i == 0) {
					line += value;
				} else {
					line += ";" + value;
				}
			}
			lines += "\n" + line;
		}

		// write
		this.write(dbTableName, lines);

	}

	/**
	 * loads all items from a file
	 */
	@Override
	public ArrayList<HashMap<String, String>> loadItems(String dbTableName) {

		ArrayList<HashMap<String, String>> out = new ArrayList<>();

		ArrayList<String[]> lines = this.getSplittedLines(dbTableName);

		if (lines.size() <= 0) {
			return out;
		}

		String[] rows = lines.get(0);
		for (int i = 1; i < lines.size(); i++) {
			// lies jede zeile
			String[] line = lines.get(i);
			// wenn zeile gleich viele params hat wie rows
			if (line.length == rows.length) {
				// die parameter der zeile in einer hashmap mit den row titlen
				// verbinden
				HashMap<String, String> hm = new HashMap<>();
				for (int k = 0; k < line.length; k++) {
					// jeden parameter in der zeile mit dem dementspechendem row
					// verbinden
					String key = rows[k];
					String value = line[k];
					hm.put(key, value);
				}
				out.add(hm);
			} else {
				// TODO schmeize irgendnen fehler
			}
		}
		return out;
	}

	@Override
	public int getNextId(String dbTableName) {
		// read first line(till;)
		ArrayList<String[]> lines = this.getSplittedLines(dbTableName);

		if (lines.size() <= 0) {
			return 0;
		}

		String[] keys = lines.get(0);
		// get id
		int id_index = -1;
		for (int i = 0; i < keys.length; i++) {
			if (keys[i].equals("id")) {
				id_index = i;
				break;
			}
		}

		if (id_index == -1) {
			// TODO: throw error that db file is wrong (no id field)
			System.out.println("ERROR the game db file has no id key. please contact the admin");
			return 0;
		}

		int id = -1;
		for (int i = 1; i < lines.size(); i++) {
			// parse to Int
			int new_id = Integer.parseInt(lines.get(i)[id_index]);
			// compare each id
			if (id < new_id) {
				id = new_id;
			}
		}
		// return biggest +1
		return id + 1;
	}

	/**
	 * returns a FileWriter for the table name. if file does not exist, it
	 * creates a new one.
	 *
	 * @param dbTableName
	 * @return
	 */
	private void write(String dbTableName, String lines) {
		FileWriter fw = null;
		File file = null;
		// FileWriter
		// try to create this file
		try {
			file = new File(dbTableName);
			fw = new FileWriter(file);
			fw.write(lines);
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * returns a scanner that can read the dbTableName File, if the file does
	 * not exist, it creates a new one
	 *
	 * @param dbTableName
	 * @return
	 */
	private Scanner getFileScanner(String dbTableName) {
		Scanner sc = null;
		File file = null;

		try {
			file = new File(dbTableName);
			sc = new Scanner(file);
		} catch (FileNotFoundException f) {
			// create new file
			this.write(dbTableName, "");

			// now create a new scanner with the created file
			try {
				sc = new Scanner(file);
			} catch (FileNotFoundException f2) {
				// eigentlich sollte das nicht eintreten, aber es muss trotzdem
				// hier drinne stehen
				f2.printStackTrace();
				System.exit(1);
			}
		}
		return sc;
	}

	private ArrayList<String[]> getSplittedLines(String dbTableName) {
		// erste zeile muss die tabellen namen enthalten
		// id;comment;place;usw...;
		Scanner sc = this.getFileScanner(dbTableName);
		ArrayList<String[]> lines = new ArrayList<>();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			// ignore some lines
			if (!(line == "") && line.contains(";")) {
				String[] params = line.split(";");
				lines.add(params);
			}
		}
		sc.close();
		return lines;
	}

	private ArrayList<String> getKeysFromHashMaps(ArrayList<HashMap<String, String>> items) {
		// erstmal alle keys bekommen, jede zeile muss alle keys haben
		ArrayList<String> keys = new ArrayList<>();
		for (HashMap<String, String> hm : items) {
			for (String key : hm.keySet()) {
				if (!keys.contains(key)) {
					keys.add(key);
				}
			}
		}
		return keys;
	}

	private String normalizeValue(String s) {
		s = s.trim();
		s = s.replace(";", "_");
		return s;
	}

	@Deprecated
	@Override
	public void updateItems(String dbTableName, ArrayList<HashMap<String, String>> items) {
		// get old data
		ArrayList<HashMap<String, String>> old = this.loadItems(dbTableName);

		for (HashMap<String, String> item : items) {
			int idx = -1;
			// find idx by id
			for (int i = 0; i < items.size(); i++) {
				HashMap<String, String> item_tmp = items.get(i);
				if (item.get("id") == item_tmp.get("id")) {
					idx = i;
					break;
				}
			}

			if (idx == -1) {
				// TODO throw error
			}

			old.set(idx, item);
		}

		this.saveItems(dbTableName, old);
	}

	@Deprecated
	@Override
	public void updateItem(String dbTableName, HashMap<String, String> item) {
		// get old data
		ArrayList<HashMap<String, String>> items = this.loadItems(dbTableName);
		int idx = -1;
		// find idx by id
		for (int i = 0; i < items.size(); i++) {
			HashMap<String, String> item_tmp = items.get(i);
			if (item.get("id") == item_tmp.get("id")) {
				idx = i;
				break;
			}
		}

		if (idx == -1) {
			// TODO throw error
		}

		items.set(idx, item);

		this.saveItems(dbTableName, items);
	}

	@Override
	public void insertItems(String dbTableName, ArrayList<HashMap<String, String>> items) {
		// TODO check ids
		// get old data
		ArrayList<HashMap<String, String>> old = this.loadItems(dbTableName);
		// Append new data
		for (HashMap<String, String> item : items) {
			old.add(item);
		}
		// save
		this.saveItems(dbTableName, old);
	}

	@Override
	public void insertItem(String dbTableName, HashMap<String, String> item) {
		// TODO check ids
		// get old data
		ArrayList<HashMap<String, String>> old = this.loadItems(dbTableName);
		// Append new data
		old.add(item);
		// save
		this.saveItems(dbTableName, old);
	}
}
