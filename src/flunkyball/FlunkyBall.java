package flunkyball;

import flunkyball.controller.GameController;
import flunkyball.controller.PlayerController;
import flunkyball.controller.TeamController;
import flunkyball.controller.TeamPlayerController;
import flunkyball.controller.UserController;
import flunkyball.db.Database;
import flunkyball.db.FileConnector;

/**
 * Summary of all needed Functionality and Controllers for FlunkyBall
 *
 * @author bergiu
 *
 */
public class FlunkyBall {
	private Database db;
	private GameController gameCtrl;
	private PlayerController playerCtrl;
	private TeamController teamCtrl;
	private TeamPlayerController teamPlayerCtrl;
	private UserController userCtrl;

	public FlunkyBall() {
		this.db = new FileConnector();
		this.gameCtrl = new GameController(this, this.db, "game.data");
		this.playerCtrl = new PlayerController(this, this.db, "player.data");
		this.teamCtrl = new TeamController(this, this.db, "team.data");
		this.teamPlayerCtrl = new TeamPlayerController(this, this.db, "team_player.data");
		this.userCtrl = new UserController(this, this.db, "user.data");
		this.initializeAll();
	}

	public GameController getGameCtrl() {
		return this.gameCtrl;
	}

	public PlayerController getPlayerCtrl() {
		return this.playerCtrl;
	}

	public TeamController getTeamCtrl() {
		return this.teamCtrl;
	}

	public TeamPlayerController getTeamPlayerCtrl() {
		return this.teamPlayerCtrl;
	}

	public UserController getUserCtrl() {
		return this.userCtrl;
	}

	public void initializeAll() {
		this.gameCtrl.initializeAllFromDB();
		this.teamCtrl.initializeAllFromDB();
		this.playerCtrl.initializeAllFromDB();
		this.teamPlayerCtrl.initializeAllFromDB();
		this.userCtrl.initializeAllFromDB();
	}

	public void saveAll() {
		this.gameCtrl.saveAllToDB();
		this.playerCtrl.saveAllToDB();
		this.teamCtrl.saveAllToDB();
		this.teamPlayerCtrl.saveAllToDB();
		this.userCtrl.saveAllToDB();
	}
}
