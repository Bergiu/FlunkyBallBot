package flunkyball.exceptions;

public class WrongFormatException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public WrongFormatException() {
		super();
	}

	public WrongFormatException(String s) {
		super(s + "\nA name has to be between three or twenty characters long, containing only" +
				" numbers, lowercase letters, spaces or the characters '_' or '-'");
	}
}
