package flunkyball.exceptions;

public class NoItemsInDBFile extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public NoItemsInDBFile() {
		super();
	}

	public NoItemsInDBFile(String s) {
		super(s);
	}
}
