package flunkyball.items;

import java.util.HashMap;

import flunkyball.FlunkyBall;

/**
 * I don't realy know if we need this, but I think it has benefits
 *
 * @author bergiu
 *
 */
public interface Item {

	/* returns the unique id of an item */
	public int getId();

	/*
	 * returns the objects data in a hashmap to save it to a database, every
	 * hashmap needs all keys, doesnt matter if it has a value
	 */
	public HashMap<String, String> toHashMap();

	/* returns a boolean that says if the object has changed its values */
	public boolean hasChanged();

	/*
	 * returns a FlunkyBall object, so that all classes can access the
	 * controllers
	 */
	public FlunkyBall getFlunkyBall();
}
