package flunkyball.items;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;

import binding.Debug;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.WrongFormatException;

public class Team extends ItemAbstract {

	private int place;
	// not null, aber random, wenn nicht angegeben
	private String teamname;

	/* foreign keys */
	private Game game;

	/* second foreign keys */
	private ArrayList<TeamPlayer> team_players;

	public Team(FlunkyBall fb, HashMap<String, String> map) {
		super(fb, map);

		if (map.containsKey("place")) {
			int place = Integer.parseInt(map.get("place"));
			this.setPlace(place);
		}

		if (map.containsKey("teamname")) {
			String teamname = map.get("teamname");
			this.setTeamname(teamname);
		} else {
			// TODO: create random teamname
			this.setTeamname(Team.getRandomTeamName());
			// throw new InvalidParameterException("A team needs a teamname");
		}

		if (map.containsKey("id_game")) {
			int id_game = Integer.parseInt(map.get("id_game"));
			Game game = (Game) this.getFlunkyBall().getGameCtrl().getItemById(id_game);
			this.setGame(game);
		} else {
			throw new InvalidParameterException("A team needs a valid game id");
		}
		Debug.print("New Team created");
	}

	public int getPlace() {
		return this.place;
	}

	public void setPlace(int place) {
		// place == 0: no winner (vllt unendschieden?)
		// place == 1: winner
		// place == 2: looser
		if (place < 0) {
			throw new InvalidParameterException("Du kannst nur den 1., 2. oder noch schlechtere Plätz belegen, du Idiot!");
		}
		this.place = place;
	}

	public String getTeamname() {
		return this.teamname;
	}

	public void setTeamname(String teamname) {
		if (!ItemAbstract.matchesNamingCondition(teamname)) {
			throw new WrongFormatException("Incorrect format.");
		}
		if (teamname == null) {
			this.teamname = Team.getRandomTeamName();
		} else {
			this.teamname = teamname;
		}
	}

	public Game getGame() {
		return this.game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public ArrayList<TeamPlayer> getTeamPlayers() {
		return this.team_players;
	}

	public void setTeamPlayers(ArrayList<TeamPlayer> team_players) {
		this.team_players = team_players;
	}

	@Override
	public HashMap<String, String> toHashMap() {
		HashMap<String, String> out = new HashMap<>();
		out.put("id", "" + this.getId());
		out.put("place", "" + this.place);
		out.put("teamname", this.teamname);
		out.put("id_game", "" + this.game.getId());
		return out;
	}

	public static String getRandomTeamName() {
		String[] names = {"Ein ","Paar ","Beispielnamen "};
		String[] namesSc = {"Als","Platzhalter"};
		int randomNumber = ThreadLocalRandom.current().nextInt(0, (names.length-1));
		int scRandomNumber = ThreadLocalRandom.current().nextInt(0, (namesSc.length-1));

		String out = names[randomNumber] + namesSc[scRandomNumber];
		return out;
	}
}
