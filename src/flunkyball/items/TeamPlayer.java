package flunkyball.items;

import java.security.InvalidParameterException;
import java.util.HashMap;

import binding.Debug;
import flunkyball.FlunkyBall;

public class TeamPlayer extends ItemAbstract {

	/* foreign keys */
	private Team team;
	private Player player;

	public TeamPlayer(FlunkyBall fb, HashMap<String, String> map) {
		super(fb, map);

		if (map.containsKey("id_team")) {
			int id_team = Integer.parseInt(map.get("id_team"));
			Team team = (Team) this.getFlunkyBall().getTeamCtrl().getItemById(id_team);
			this.setTeam(team);
		} else {
			throw new InvalidParameterException("A TeamPlayer needs a valid team id");
		}

		if (map.containsKey("id_player")) {
			int id_player = Integer.parseInt(map.get("id_player"));
			Player player = (Player) this.getFlunkyBall().getPlayerCtrl().getItemById(id_player);
			this.setPlayer(player);
		} else {
			throw new InvalidParameterException("A TeamPlayer needs a valid player id");
		}
		Debug.print("New Team created");
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
	public HashMap<String, String> toHashMap() {
		HashMap<String, String> out = new HashMap<>();
		out.put("id", "" + this.getId());
		out.put("id_team", "" + this.team.getId());
		out.put("id_player", "" + this.player.getId());
		return out;
	}

}
