package flunkyball.items;

import java.util.HashMap;

import binding.Debug;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;
import flunkyball.exceptions.WrongFormatException;

/**
 * One User that is authorized and connected with Telegram/Email and belongs to
 * one Player
 *
 * @author bergiu
 *
 */
public class User extends ItemAbstract {

	private String email;
	private String username;
	private String telegram_id;

	/* foreign keys */
	private Player player;

	public User(FlunkyBall fb, HashMap<String, String> map) {
		super(fb, map);

		if (map.containsKey("email")) {
			String email = map.get("email");
			this.setEmail(email);
		}

		if (map.containsKey("username")) {
			String username = map.get("username");
			this.setUsername(username);
		} else {
			throw new InvalidParametersException("A user needs an username");
		}

		if (map.containsKey("telegram_id")) {
			String telegram_id = map.get("telegram_id");
			this.setTelegramId(telegram_id);
		}

		if (map.containsKey("id_player")) {
			if (!map.get("id_player").equals("")) {
				int id_player = Integer.parseInt(map.get("id_player"));
				Player player = (Player) this.getFlunkyBall().getPlayerCtrl().getItemById(id_player);
				this.setPlayer(player);
			}
		} else {
			// muss keinen fehler werfen, da player = null sein darf
		}
		Debug.print("New User registered");
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		if (!ItemAbstract.matchesNamingCondition(username) || (username == null) || (username == "")) {
			throw new WrongFormatException("Der Username hat ein falsches Format.");
		}
		this.username = username;
	}

	public String getTelegramId() {
		return this.telegram_id;
	}

	public void setTelegramId(String telegram_id) {
		this.telegram_id = telegram_id;
	}

	public Player getPlayer() {
		return this.player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	@Override
	public HashMap<String, String> toHashMap() {
		HashMap<String, String> out = new HashMap<>();
		out.put("id", "" + this.getId());
		out.put("email", this.email);
		out.put("username", this.username);
		out.put("telegram_id", this.telegram_id);
		if (this.player != null) {
			out.put("id_player", "" + this.player.getId());
		} else {
			out.put("id_player", null);
		}
		return out;
	}
}
