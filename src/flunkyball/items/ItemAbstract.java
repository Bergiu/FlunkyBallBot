package flunkyball.items;

import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;

public abstract class ItemAbstract implements Item {

	private FlunkyBall fb;
	private int id;
	private boolean changed;

	public ItemAbstract(FlunkyBall fb, int id) {
		this.fb = fb;
		this.id = id;
		this.changed = false;
	}

	public ItemAbstract(FlunkyBall fb, HashMap<String, String> map) {
		this.fb = fb;
		if (map.containsKey("id")) {
			this.id = Integer.parseInt(map.get("id"));
		} else {
			throw new InvalidParametersException("Every item needs an id");
		}
		this.changed = false;
	}

	@Override
	public int getId() {
		return this.id;
	}

	protected void setChanged(boolean changed) {
		this.changed = changed;
	}

	@Override
	public boolean hasChanged() {
		return this.changed;
	}

	@Override
	public FlunkyBall getFlunkyBall() {
		return this.fb;
	}

	public static boolean matchesNamingCondition(String s) {
		return s.toLowerCase().matches("([a-z]|[0-9]|_|-| ){3,20}");
	}

}