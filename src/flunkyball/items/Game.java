package flunkyball.items;

import java.util.ArrayList;
import java.util.HashMap;

import binding.Debug;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;

/**
 * Represents one FlunkyBall Game. You can add a place where you played this
 * game, comment it and set a date and time.
 *
 * @author bergiu
 *
 */
public class Game extends ItemAbstract {

	private String comment;
	private String place;
	// not null
	private String time;
	// not null
	private String date;

	/* second foreign keys */
	private ArrayList<Team> teams;

	public Game(FlunkyBall fb, HashMap<String, String> map) {
		super(fb, map);

		if (map.containsKey("comment")) {
			String comment = map.get("comment");
			this.setComment(comment);
		}

		if (map.containsKey("place")) {
			String place = map.get("place");
			this.setPlace(place);
		}

		if (map.containsKey("time")) {
			String time = map.get("time");
			this.setTime(time);
		} else {
			throw new InvalidParametersException("A game needs a time");
		}

		if (map.containsKey("date")) {
			String date = map.get("date");
			this.setDate(date);
		} else {
			throw new InvalidParametersException("A game needs a date");
		}
		Debug.print("New Game created");
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		// System.out.println(place);
		// if (place == "" || place == null){
		// this.place = "";
		// } else {
		// if (!ItemAbstract.matchesNamingCondition(place)) {
		// throw new WrongFormatException("Der Place hat ein falsches Format.");
		// }
		// }
		this.place = place;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public ArrayList<Team> getTeams() {
		return this.teams;
	}

	public void setTeams(ArrayList<Team> teams) {
		this.teams = teams;
	}

	@Override
	public HashMap<String, String> toHashMap() {
		// TODO Auto-generated method stub
		HashMap<String, String> out = new HashMap<>();
		out.put("id", "" + this.getId());
		out.put("comment", this.comment);
		out.put("place", this.place);
		out.put("time", this.time);
		out.put("date", this.date);
		return out;
	}

}
