package flunkyball.items;

import java.util.ArrayList;
import java.util.HashMap;

import binding.Debug;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;
import flunkyball.exceptions.WrongFormatException;

/**
 * Represents one Player in the Games. Every person who ever played FlunkyBall
 * should be in here ;) A player doesn't need an account, but if he wants he has to
 * assign his player object to a user object.
 *
 * @author bergiu
 *
 */
public class Player extends ItemAbstract {
	private String name;

	/* second foreign keys */
	private User user;
	private ArrayList<TeamPlayer> team_players;

	/* calculated */
	private int score;

	public Player(FlunkyBall fb, HashMap<String, String> map) {
		super(fb, map);

		if (map.containsKey("name")) {
			String name = map.get("name");
			this.setName(name);
		} else {
			throw new InvalidParametersException("A player needs a name");
		}
		Debug.print("New Player created");
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		if (!ItemAbstract.matchesNamingCondition(name) || (name == null)) {
			throw new WrongFormatException("Incorrect format.");
		}
		this.name = name;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ArrayList<TeamPlayer> getTeam_players() {
		return this.team_players;
	}

	public void setTeam_players(ArrayList<TeamPlayer> team_players) {
		this.team_players = team_players;
	}

	public void calcScore() {
		// TODO calculate score
		this.score = 3;
	}

	public int getScore() {
		return this.score;
	}

	@Override
	public HashMap<String, String> toHashMap() {
		HashMap<String, String> out = new HashMap<>();
		out.put("id", "" + this.getId());
		out.put("name", this.name);
		return out;
	}

}
