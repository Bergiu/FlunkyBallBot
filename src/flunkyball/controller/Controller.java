package flunkyball.controller;

import java.util.ArrayList;
import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.items.Item;

public interface Controller {

	/* Creates an Item but does not save it */
	public Item makeItem(HashMap<String, String> map);

	/* Creates an Item and saves it */
	// uses db.getNextId() for the process
	public Item createItem(HashMap<String, String> map);

	/* Adds an item to the itemlist */
	public void addItem(Item item);

	/* Removes an item from the itemslist */
	// public void removeItem(Item item);

	/* returns one item with the specified id */
	public Item getItemById(int id);

	/* returns all items from the controller */
	public ArrayList<Item> getAllItems();

	/* to access other controllers */
	public FlunkyBall getFB();

	/*
	 * returns the name of the database table, should be unique for each
	 * controller
	 */
	public String getDBTableName();

	/* returns the database */
	public Database getDB();

	/* saves all items that has changed to the database */
	public void updateDB();

	/* greift auf die datenbank zu und lädt alle items */
	public void initializeAllFromDB();

	/* greift auf die datenbank zu und speichert alle items */
	public void saveAllToDB();

}
