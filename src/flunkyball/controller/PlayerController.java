package flunkyball.controller;

import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.exceptions.ItemNotFoundException;
import flunkyball.items.Item;
import flunkyball.items.Player;

public class PlayerController extends ControllerAbstract {

	public PlayerController(FlunkyBall fb, Database db, String dbTableName) {
		super(fb, db, dbTableName);
	}

	@Override
	public Item makeItem(HashMap<String, String> map) {
		Item item = new Player(this.getFB(), map);
		// this.addItem(item);
		return item;
	}

	public Item getPlayerByName(String name) {
		for (Item item : this.getAllItems()) {
			Player player = (Player) item;
			if (name.equals(player.getName())) {
				return player;
			}
		}
		throw new ItemNotFoundException();
	}
}
