package flunkyball.controller;

import java.util.ArrayList;
import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.items.Game;
import flunkyball.items.Item;
import flunkyball.items.Team;

public class TeamController extends ControllerAbstract {

	public TeamController(FlunkyBall fb, Database db, String dbTableName) {
		super(fb, db, dbTableName);
	}

	@Override
	public Item makeItem(HashMap<String, String> map) {
		Item item = new Team(this.getFB(), map);
		// this.addItem(item);
		return item;
	}

	public ArrayList<Team> getAllTeamsFromGame(Game game) {
		ArrayList<Team> out = new ArrayList<>();
		for (Item item : this.getAllItems()) {
			Team team = (Team) item;
			if (team.getGame() == game) {
				out.add(team);
			}
		}
		return out;
	}
}
