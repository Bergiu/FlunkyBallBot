package flunkyball.controller;

import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.items.Game;
import flunkyball.items.Item;

public class GameController extends ControllerAbstract {

	public GameController(FlunkyBall fb, Database db, String dbTableName) {
		super(fb, db, dbTableName);
	}

	@Override
	public Item makeItem(HashMap<String, String> map) {
		Item item = new Game(this.getFB(), map);
		// this.addItem(item);
		return item;
	}

}
