package flunkyball.controller;

import java.util.ArrayList;
import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.items.Item;
import flunkyball.items.Player;
import flunkyball.items.Team;
import flunkyball.items.TeamPlayer;

public class TeamPlayerController extends ControllerAbstract {

	public TeamPlayerController(FlunkyBall fb, Database db, String dbTableName) {
		super(fb, db, dbTableName);
	}

	@Override
	public Item makeItem(HashMap<String, String> map) {
		Item item = new TeamPlayer(this.getFB(), map);
		// this.addItem(item);
		return item;
	}

	public ArrayList<Player> getAllPlayersFromTeam(Team team) {
		ArrayList<Player> out = new ArrayList<>();
		for (Item item : this.getAllItems()) {
			TeamPlayer teamPlayer = (TeamPlayer) item;
			if (teamPlayer.getTeam() == team) {
				out.add(teamPlayer.getPlayer());
			}
		}
		return out;
	}

	public ArrayList<Team> getAllTeamFromPlayer(Player player) {
		ArrayList<Team> out = new ArrayList<>();
		for (Item item : this.getAllItems()) {
			TeamPlayer teamPlayer = (TeamPlayer) item;
			if (teamPlayer.getPlayer() == player) {
				out.add(teamPlayer.getTeam());
			}
		}
		return out;
	}
}
