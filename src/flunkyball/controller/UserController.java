package flunkyball.controller;

import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.exceptions.IdNotFoundException;
import flunkyball.items.Item;
import flunkyball.items.User;

public class UserController extends ControllerAbstract {

	public UserController(FlunkyBall fb, Database db, String dbTableName) {
		super(fb, db, dbTableName);
	}

	@Override
	public Item makeItem(HashMap<String, String> map) {
		Item item = new User(this.getFB(), map);
		// this.addItem(item);
		return item;
	}

	public User getUserByTelegramId(int tg_id){
		for(Item item: this.getAllItems()){
			User user = (User) item;
			if(user.getTelegramId().equals(""+tg_id)){
				return user;
			}
		}
		throw new IdNotFoundException("No User with this TelegramId");
	}
}
