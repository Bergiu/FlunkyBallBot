package flunkyball.controller;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import flunkyball.FlunkyBall;
import flunkyball.db.Database;
import flunkyball.exceptions.IdNotFoundException;
import flunkyball.items.Item;

public abstract class ControllerAbstract implements Controller {
	private ArrayList<Item> items;
	private String dbTableName;
	private FlunkyBall fb;
	private Database db;

	public ControllerAbstract(FlunkyBall fb, Database db, String dbTableName) {
		this.fb = fb;
		this.db = db;
		this.dbTableName = dbTableName;

		this.initializeAllFromDB();
	}

	@Override
	public String getDBTableName() {
		return this.dbTableName;
	}

	@Override
	public FlunkyBall getFB() {
		return this.fb;
	}

	@Override
	public Database getDB() {
		return this.db;
	}

	@Override
	public Item getItemById(int id) {
		for (Item item : this.items) {
			if (item.getId() == id) {
				return item;
			}
		}
		throw new IdNotFoundException("There is no User with the id: "+id);
	}

	@Override
	public ArrayList<Item> getAllItems() {
		return this.items;
	}

	@Override
	public void addItem(Item item) {
		this.items.add(item);
	}

	@Deprecated
	@Override
	public void updateDB() {
		ArrayList<HashMap<String, String>> items_tmp = new ArrayList<>();
		for (Item item : this.items) {
			if (item.hasChanged()) {
				HashMap<String, String> item_tmp = item.toHashMap();
				items_tmp.add(item_tmp);
			}
		}
		this.db.updateItems(this.dbTableName, items_tmp);
	}

	@Override
	public void initializeAllFromDB() {
		ArrayList<HashMap<String, String>> items_tmp = this.db.loadItems(this.dbTableName);
		ArrayList<Item> items = new ArrayList<>();
		for (HashMap<String, String> item_tmp : items_tmp) {
			Item item = this.makeItem(item_tmp);
			items.add(item);
		}
		this.items = items;
	}

	@Override
	public void saveAllToDB() {
		ArrayList<HashMap<String, String>> items_tmp = new ArrayList<>();
		for (Item item : this.items) {
			HashMap<String, String> item_tmp = item.toHashMap();
			items_tmp.add(item_tmp);
		}
		this.db.saveItems(this.dbTableName, items_tmp);
	}

	@Override
	public Item createItem(HashMap<String, String> map) {
		if (map.containsKey("id")) {
			throw new InvalidParameterException("You can not choose the id of new items yourself");
		}
		int id = this.getDB().getNextId(this.getDBTableName());
		map.put("id", "" + id);
		Item item = this.makeItem(map);
		this.getDB().insertItem(this.getDBTableName(), item.toHashMap());
		this.addItem(item);
		return item;
	}
}
