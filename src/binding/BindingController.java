package binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import binding.Task.TaskOutput;
import binding.exceptions.AllTasksCompletedException;
import binding.exceptions.BindingException;
import binding.views.NewGameBinding;
import binding.views.Views;
import binding.views.ViewsInput;

import flunkyball.FlunkyBall;
import flunkyball.exceptions.IdNotFoundException;
import flunkyball.exceptions.WrongFormatException;
import flunkyball.items.ItemAbstract;
import flunkyball.items.User;

public class BindingController {

	FlunkyBall fb;
	ArrayList<Binding> bindings;
	HashMap<String, MultiLineBinding> aktuelleBindings;

	public BindingController(FlunkyBall fb) {
		this.fb = fb;
		this.bindings = new ArrayList<>();
		this.aktuelleBindings = new HashMap<>();
		//System.out.print

		/* *****************
		 *
		 * OneLineBindings:
		 *
		 * *****************/
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.listPlayersInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.listPlayers(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.listGamesInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.listGames(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.showGameInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.showGame(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.newPlayerInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.newPlayer(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.newPlayerWrongFormatInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.newPlayerWrongFormat(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.setUsernameInput ) {
			@Override
			public TaskOutput main(String command) {
				return Views.setUsername(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.setUsernameWrongFormatInput ) {
			@Override
			public TaskOutput main(String command) {
				return Views.setUsernameWrongFormat(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.setEmailInput ) {
			@Override
			public TaskOutput main(String command) {
				return Views.setEmail(this.getFB(), command);
			}
		});
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.setEmailWrongFormatInput ) {
			@Override
			public TaskOutput main(String command) {
				return Views.setEmailWrongFormat(this.getFB(), command);
			}
		});

		/* *******************
		 *
		 * MultilineBindings:
		 *
		 * *******************/
		this.bindings.add(new NewGameBinding(this.fb, ViewsInput.newGameInputMLB));

		/* ************
		 *
		 * HELP method
		 *
		 * ************/
		// help binding, muss als letztes (außer dem default) gesetzt werden
		this.bindings.add(new OneLineBinding(this.fb, ViewsInput.helpInput) {
			@Override
			public TaskOutput main(String command) {
				return Views.help(this.getFB(), command, BindingController.this.bindings);
			}
		});

	}

	public void runCmd() {
		// input wird eingelesen und geht durch das array durch
		User user = (User) this.fb.getUserCtrl().getItemById(0);
		Scanner sc = new Scanner(System.in);
		while (true) {
			String line = sc.nextLine();
			if(line.equals("cmd exit")){
				break;
			}
			this.execute(line, user, "console");
		}
		sc.close();
	}

	/**
	 * Executes the binding that matches the command and saves MultiLineBindings
	 * in the specified MultiLineId
	 * 
	 * @param command
	 * @param user
	 * @param multiLineId
	 * @return
	 */
	public TaskOutput execute(String command, User user, String multiLineId) {
		// erst gucken ob user ein multiline binding gestartet hat
		if (aktuelleBindings.containsKey(multiLineId)) {

			MultiLineBinding aktuellesBinding = this.aktuelleBindings.get(multiLineId);
			if(Tools.lowerCaseMatchesArray(command, ViewsInput.cancel)){
				aktuellesBinding.cancel();
			} else {
				try {
					TaskOutput out = aktuellesBinding.execute(command);
					if (aktuellesBinding.isCompleted()) {
						this.aktuelleBindings.remove(multiLineId);
					}
					return out;
				} catch (AllTasksCompletedException atce) {
					// just to be 100% sure that it is beeing removed after all
					// tasks are completed, i think it should not be necessary
					// anymore
					this.aktuelleBindings.remove(multiLineId);
				}
			}

		} else {
			// no multiLineBinding is started
			for (Binding bnd : this.bindings) {
				try {
					if (bnd.matches(command)) {
						// it matches, so execute this binding
						if (bnd instanceof MultiLineBinding) {
							// bei einem multiLineBinding muss erst ein clone
							// gespeichert werden, welcher dann ausgeführt wird,
							// damit das original unangetastet bleibt
							MultiLineBinding mlb = (MultiLineBinding) bnd;
							MultiLineBinding mlb_new = mlb.clone();
							this.aktuelleBindings.put(multiLineId, mlb_new);
							TaskOutput response = mlb_new.execute(command);
							return response;
						} else {
							TaskOutput response = bnd.execute(command);
							return response;
						}
					}
				} catch (BindingException be) {
					// it does not match, this is just to be sure that there are
					// no problems, i think it is not necessary anymore
				}
			}
		}
		return new TaskOutput("No command found: " + command + "\nTry /help to get help", null, null);
		// fb.saveAll();
	}

	public SendMessage executeTelegram(int tg_id, long chat_id, String text, Message message) {
		User user;
		try {
			user = this.getUserOrCreateFromTelegram(tg_id, message);
		} catch (WrongFormatException wfe) {
			// der username hat ein falsches format
			// TODO: view -> sign up
			text = "You have to /set_username first, to use this bot";
			SendMessage sendMessageRequest = new SendMessage();
			sendMessageRequest.setChatId(message.getChatId().toString());
			sendMessageRequest.setText(text);
			return sendMessageRequest;
		}

		String multiLineId = tg_id + "" + chat_id;

		TaskOutput out = this.execute(text, user, multiLineId);

		SendMessage sendMessageRequest = new SendMessage();
		sendMessageRequest.setChatId(message.getChatId().toString());
		sendMessageRequest.disableNotification();
		sendMessageRequest.enableHtml(true);
		sendMessageRequest.setParseMode("HTML");
		sendMessageRequest.setReplyToMessageId(message.getMessageId());

		sendMessageRequest.setText(out.getText());
		if (out.replyMarkup != null) {
			sendMessageRequest.setReplyMarkup(out.replyMarkup);
		}
		return sendMessageRequest;
	}

	public User getUserOrCreateFromTelegram(int tg_id, Message message) {
		User user;
		try {
			user = this.fb.getUserCtrl().getUserByTelegramId(tg_id);
		} catch (IdNotFoundException infe) {
			try {
				String username = message.getFrom().getFirstName() + message.getFrom().getLastName();
				if (username == "" || !ItemAbstract.matchesNamingCondition(username)) {
					username = message.getFrom().getUserName();
				}
				if (username == "" || !ItemAbstract.matchesNamingCondition(username)) {
					username = message.getFrom().getId() + "";
				}
				HashMap<String, String> map = new HashMap<>();
				map.put("telegram_id", "" + tg_id);
				map.put("username", username);
				user = (User) this.fb.getUserCtrl().createItem(map);
			} catch (IdNotFoundException infe2) {
				// keine ahnung, wahrscheinlich ist der
				// fremdschlüssel nach player falsch
				// hoffentlich passiert das einfach nie :/
				System.out.println("ERROR: Main.infe2 while creating new user");
				throw infe2;
			}
		}
		return user;
	}

}
/**
 * Garbage der vllt noch gebraucht wird
 */

// bindings.add(new OneLineBinding(fb, Views.listTeamsInput) {
// @Override
// public TaskOutput main(String command) {
// return Views.listTeams(this.getFB(), command);
// }
// });

// bindings.add(new OneLineBinding(fb, Views.setTeamNameInput) {
// @Override
// public TaskOutput main(String command) {
// return Views.setTeamName(this.getFB(), command);
// }
// });
