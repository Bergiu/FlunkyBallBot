package binding.views;

import java.util.ArrayList;
import java.util.HashMap;

import binding.Binding;
import binding.Task.TaskOutput;
import binding.Tools;
import flunkyball.FlunkyBall;
import flunkyball.items.Game;
import flunkyball.items.Item;
import flunkyball.items.Player;
import flunkyball.items.Team;
import flunkyball.items.User;

public class Views {

	/**
	 * Gibt eine Hilfe-Nachricht aus, muss aber als letztes Binding eingefügt
	 * werden (bzw. als vorletztes, wenn es ein default Binding gibt)
	 *
	 * @param fb
	 * @param command
	 * @param bindings
	 * @return
	 */
	public static TaskOutput help(FlunkyBall fb, String command, ArrayList<Binding> bindings) {
		// HELP Message
		String out_text = "Help: \n\n Available Commands:\n";
		for (Binding binding : bindings) {
			for (int i = 0; i < binding.getMatches().length; i++) {
				if (i == 0) {
					out_text += String.format("  - %s\n", binding.getMatches()[i]);
				} else if (i == 1) {
					out_text += String.format("    Aliasse:\n      %s\n", binding.getMatches()[i]);
				} else {
					out_text += String.format("      %s\n", binding.getMatches()[i]);
				}
			}
		}
		return new TaskOutput(out_text, null, null);
	}

	/* ************************
	 *
	 * GAMES getter und setter
	 *
	 * ************************/

	public static TaskOutput listGames(FlunkyBall fb, String command) {
		String out_text = "";
		for (Item item : fb.getGameCtrl().getAllItems()) {
			Game game = (Game) item;
			ArrayList<Team> teams = fb.getTeamCtrl().getAllTeamsFromGame(game);
			if (teams.size() == 2) {
				Team team1 = teams.get(0);
				Team team2 = teams.get(1);
				out_text += String.format("Game %d - %s vs. %s\n", game.getId(), team1.getTeamname(), team2.getTeamname());
			} else if (teams.size() > 2) {
				out_text += String.format("Game %d - mehrere Teams\n", game.getId());
			} else {
				out_text += String.format("Game %d - zu wenig Teams\n", game.getId());
			}
		}
		return new TaskOutput(out_text, null, null);
	}

	public static TaskOutput showGame(FlunkyBall fb, String command) {
		String out_text = "";
		command = command.toLowerCase();
		String id_tmp = "";
		for (String regex : ViewsInput.showGameInput) {
			if (Tools.lowerCaseMatches(command, regex)) {
				id_tmp = Tools.getRegExGroup(command, regex, 1);
			}
		}
		int id = Integer.parseInt(id_tmp);
		Item item = fb.getGameCtrl().getItemById(id);
		Game game = (Game) item;
		out_text += String.format("Game %d:\n  Date: %s\n  Time: %s\n", game.getId(), game.getDate(), game.getTime());
		if (game.getPlace() != "") {
			out_text += String.format("  Place: %s\n", game.getPlace());
		}
		if (game.getComment() != "") {
			out_text += String.format("  Comment: %s\n", game.getComment());
		}
		out_text += "\nTeams:\n";
		ArrayList<Team> teams = fb.getTeamCtrl().getAllTeamsFromGame(game);
		for (int i = 0; i < teams.size(); i++) {
			Team team = teams.get(i);
			if (team.getPlace() == 0) {
				out_text += String.format("%s:\n", team.getTeamname());
			} else if (team.getPlace() == 1) {
				out_text += String.format("%s (Sieger):\n", team.getTeamname());
			} else {
				out_text += String.format("%s (Verlierer):\n", team.getTeamname());
			}
			ArrayList<Player> players = fb.getTeamPlayerCtrl().getAllPlayersFromTeam(team);
			if (players.size() == 0) {
				out_text += "No players in this team :/\n";
			} else {
				for (Player player : players) {
					out_text += String.format("  %s (Score: %d)\n", player.getName(), player.getScore());
				}
			}
		}
		return new TaskOutput(out_text, null, null);
	}

	/* *************************
	 *
	 * Player getter und setter
	 *
	 * *************************/

	public static TaskOutput listPlayers(FlunkyBall fb, String command) {
		String out_text = "";
		for (Item item : fb.getPlayerCtrl().getAllItems()) {
			Player player = (Player) item;
			out_text += String.format("%s (Score: %d)\n", player.getName(), player.getScore());
		}
		return new TaskOutput(out_text, null, null);
	}

	public static TaskOutput showProfile(FlunkyBall fb, String command){
		// TODO
		return null;
	}

	public static TaskOutput newPlayer(FlunkyBall fb, String command) {
		//XXX
		String player_name = "";
		for (String regex : ViewsInput.newPlayerInput) {
			if (Tools.lowerCaseMatches(command, regex)) {
				player_name = Tools.getRegExGroup(command, regex, 1);
			}
		}
		HashMap<String,String> map = new HashMap<>();
		map.put("name", player_name);
		Player player = (Player) fb.getPlayerCtrl().createItem(map);
		String out_text = "New Player "+player.getName()+" created.";
		return new TaskOutput(out_text,null,null);
	}

	/* ***********************
	 *
	 * USER getter und setter
	 *
	 * ***********************/

	/*
	 * Sets the username of the specified user
	 */
	public static TaskOutput setUsername(FlunkyBall fb, String command){
		//XXX: change id with an hash
		String new_username = "";
		int user_id = -1;
		for (String regex: ViewsInput.setUsernameInput) {
			if(Tools.lowerCaseMatches(command, regex)) {
				String tmp_user_id = Tools.getRegExGroup(command, regex, 1);
				user_id = Integer.parseInt(tmp_user_id);
				new_username = Tools.getRegExGroup(command, regex, 2);
			}
		}
		User user = (User) fb.getUserCtrl().getItemById(user_id);
		String old_username = user.getUsername();
		user.setUsername(new_username);

		String out_text = String.format("Username changed from %s to %s.",old_username,new_username);
		return new TaskOutput(out_text,null,null);
	}

	public static TaskOutput setEmail(FlunkyBall fb, String command){
		String new_email = "";
		int user_id = -1;
		for (String regex: ViewsInput.setEmailInput){
			String tmp_user_id = Tools.getRegExGroup(command, regex, 1);
			user_id = Integer.parseInt(tmp_user_id);
			new_email = Tools.getRegExGroup(command, regex, 2);
		}
		User user = (User) fb.getUserCtrl().getItemById(user_id);
		String old_email = user.getEmail();
		user.setEmail(new_email);

		if(old_email==null||old_email.equals("")){
			old_email = "no email";
		}

		String out_text = String.format("Email changed from %s to %s.", old_email, new_email);
		return new TaskOutput(out_text,null,null);
	}

	/* **************
	 *
	 * WRONG FORMATS
	 * ( for those commands that needs an special syntax )
	 *
	 * **************/

	public static TaskOutput newPlayerWrongFormat(FlunkyBall fb, String command){
		//TODO
		return null;
	}

	public static TaskOutput setUsernameWrongFormat(FlunkyBall fb, String command){
		String out_text = "You have to type something like this:\n";
		String id = Tools.getRegExGroupArray(command, ViewsInput.setUsernameWrongFormatInput, 1);
		if(id==null){
			out_text += "/set_username_YOURID YOURNAME";
		} else {
			out_text += "/set_username_"+id+" YOURNAME";
		}
		return new TaskOutput(out_text,null,null);
	}

	public static TaskOutput setEmailWrongFormat(FlunkyBall fb, String command){
		String id = Tools.getRegExGroupArray(command, ViewsInput.setEmailWrongFormatInput, 1);
		String out_text = "You have to type something like this:\n";
		if(id==null){
			out_text += "/set_email_YOURID YOUR@EMAIL.ADRESS";
		} else {
			out_text += "/set_email_"+id+" YOUR@EMAIL.ADRESS";
		}
		return new TaskOutput(out_text,null,null);
	}

}
