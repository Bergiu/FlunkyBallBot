package binding.views;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import binding.MultiLineBinding;
import binding.Task;
import binding.Task.TaskOutput;
import binding.exceptions.AllTasksCompletedException;
import flunkyball.FlunkyBall;
import flunkyball.items.Game;
import flunkyball.items.Item;
import flunkyball.items.Team;

public class NewGameBinding extends MultiLineBinding {

	private Task team1Task;
	private Task players1Task;
	private Task team2Task;
	private Task players2Task;

	private Game game;

	private Team team1;
	private Team team2;

	public NewGameBinding(FlunkyBall fb, String[] matches) {
		super(fb, matches);
	}

	public NewGameBinding(NewGameBinding ngb) {
		super(ngb);
	}

	// hier müssen jetzt die tasks initialisiert werden
	@Override
	public TaskOutput init(String command) {
		// initialize all tasks
		this.team1Task = new NewTeamTask(this.getFB());
		this.players1Task = new AddPlayersToTeamTask(this.getFB());
		this.team2Task = new NewTeamTask(this.getFB());
		this.players2Task = new AddPlayersToTeamTask(this.getFB());

		// get all attributes
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Date date_tmp = new Date();
		String date = dateFormat.format(date_tmp); // 2016/11/16
		String time = timeFormat.format(date_tmp); // 12:08:43

		HashMap<String, String> map = new HashMap<>();
		map.put("time", time);
		map.put("date", date);
		this.game = (Game) this.getFB().getGameCtrl().createItem(map);
		// TODO: comment and place in a new task

		// generate return
		// String out = "Where have you played?";
		// execute first task
		HashMap<String, Item> requiredItems = new HashMap<>();
		requiredItems.put("game", this.game);
		this.team1Task.setRequiredItems(requiredItems);
		TaskOutput out = this.team1Task.execute(command);
		if (out.generatedItems.containsKey("team")) {
			this.team1 = (Team) out.generatedItems.get("team");
		}
		HashMap<String, Item> map_out = new HashMap<>();
		return new TaskOutput(out.getText(), map_out, out.replyMarkup);
	}

	// executes all tasks
	// TODO: WTF make it readable
	@Override
	public TaskOutput main(String command) {
		if (!this.team1Task.isCompleted()) {
			HashMap<String, Item> map = new HashMap<>();
			map.put("game", this.game);
			this.team1Task.setRequiredItems(map);
			TaskOutput out = this.team1Task.execute(command);
			if (out.generatedItems.containsKey("team")) {
				this.team1 = (Team) out.generatedItems.get("team");
			}
			if (this.team1Task.isCompleted()) {
				// initialize the next task
				TaskOutput out2 = this.players1Task.execute(command);
				out.addTextAfter(out2.getText());
				out.addGeneratedItems(out2.generatedItems);
				out.addReplyMarkup(out2.replyMarkup); //add or replace??
				return out;
			}
			return out;

		} else if (!this.players1Task.isCompleted()) {
			HashMap<String, Item> map = new HashMap<>();
			map.put("team", this.team1);
			this.players1Task.setRequiredItems(map);
			TaskOutput out = this.players1Task.execute(command);
			if (this.players1Task.isCompleted()) {
				// initialize the next task
				map.put("game", this.game);
				this.team2Task.setRequiredItems(map);
				TaskOutput out2 = this.team2Task.execute(command);
				out.addTextAfter(out2.getText());
				out.addGeneratedItems(out2.generatedItems);
				out.addReplyMarkup(out2.replyMarkup); //add or replace??
				return out;
			}
			return out;

		} else if (!this.team2Task.isCompleted()) {
			HashMap<String, Item> map = new HashMap<>();
			map.put("game", this.game);
			this.team2Task.setRequiredItems(map);
			TaskOutput out = this.team2Task.execute(command);
			if (out.generatedItems.containsKey("team")) {
				this.team2 = (Team) out.generatedItems.get("team");
			}
			if (this.team2Task.isCompleted()) {
				// initialize the next task
				TaskOutput out2 = this.players2Task.execute(command);
				out.addTextAfter(out2.getText());
				out.addGeneratedItems(out2.generatedItems);
				out.addReplyMarkup(out2.replyMarkup); //add or replace??
				return out;
			}
			return out;

		} else if (!this.players2Task.isCompleted()) {
			HashMap<String, Item> map = new HashMap<>();
			map.put("team", this.team2);
			this.players2Task.setRequiredItems(map);
			TaskOutput out = this.players2Task.execute(command);
			if (this.players2Task.isCompleted()) {
				this.setCompleted();
			}
			return out;

		} else {
			throw new AllTasksCompletedException();
		}

	}

	@Override
	public TaskOutput exit(TaskOutput out) {
		return out;
	}

	@Override
	public TaskOutput cancel(){
		return new TaskOutput("",null,null);
	}
	
	@Override
	public MultiLineBinding clone(){
		return new NewGameBinding(this);
	}
}
