package binding.views;

import java.util.ArrayList;
import java.util.HashMap;

import binding.Task;
import binding.Tools;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;
import flunkyball.exceptions.WrongFormatException;
import flunkyball.items.Game;
import flunkyball.items.Item;
import flunkyball.items.Team;

/**
 * hm must contain a "game"
 */
public class NewTeamTask extends Task {

	public NewTeamTask(FlunkyBall fb) {
		super(fb);
	}

	@Override
	public TaskOutput init() {
		// check required items
		if (!this.getRequiredItems().containsKey("game")) {
			throw new InvalidParametersException();
		}

		// get all attributes
		Game game = (Game) this.getRequiredItems().get("game");
		ArrayList<Team> teams = this.getFB().getTeamCtrl().getAllTeamsFromGame(game);
		int teamnumber = teams.size() + 1;

		// generate return
		String out_text = "Enter a name for the Team " + teamnumber;
		HashMap<String, Item> out_map = new HashMap<>();
		this.setLastTaskOutput(new TaskOutput(out_text, out_map, null));
		return this.getLastTaskOutput();
	}

	// creates a new team assigned to the given game
	@Override
	public TaskOutput main(String command) {
		// check required items
		if (!this.getRequiredItems().containsKey("game")) {
			throw new InvalidParametersException();
		}

		String teamname;
		// get all attributes
		if(Tools.lowerCaseMatchesArray(command, ViewsInput.randomName)){
			teamname = "";
		} else {
			teamname = command;
		}
		Game game = (Game) this.getRequiredItems().get("game");

		// generate hashMap and Item
		HashMap<String, String> map = new HashMap<>();
		System.out.println(game.getId());
		map.put("id_game", "" + game.getId());
		map.put("teamname", teamname);

		Team team;
		try {
			team = (Team) this.getFB().getTeamCtrl().createItem(map);
		} catch (WrongFormatException wfe) {
			//this is an error, so display last output + error message
			this.getLastTaskOutput().addTextAfter("<b>"+wfe.getMessage()+"</b>");
			return this.getLastTaskOutput();
		}

		// generate return
		String out_text = "New Team added";
		HashMap<String, Item> out_map = new HashMap<>();
		out_map.put("team", team);
		this.setCompleted();
		return new TaskOutput(out_text, out_map, null);
	}
}