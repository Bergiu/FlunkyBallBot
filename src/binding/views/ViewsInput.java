package binding.views;

public class ViewsInput {

	/* *********************
	 *
	 * Other Global Strings
	 *
	 * *********************/

	public static String[] cancel = new String[] { "/cancel", "Cancel" };

	public static String[] nextTask = new String[] { "/next_task", "Next Task" };

	public static String[] randomName = new String[] { "/random_name", "Random Name" };


	/* *****************************
	 *
	 * Inputs für MultiLineBindings
	 *
	 * *****************************/

	public static String[] newGameInputMLB = new String[] { "create Game", "new Game", "/new_game", "/create_game" };

	//TODO: implement
	public static String[] newRandomGameInputMLB = new String[] { "create random Game", "new random Game", "/new_game_random", "/new_random_game", "/create_game_random", "/create_random_game" };

	//TODO: implement
	public static String[] newPlayerInputMLB = new String[] { "create Player", "create new Player", "/create_player", "/create_new_player" };

	//TODO: implement
	public static String[] selectPlayerInputMLB = new String[] { "set Player", "/set_player" };

	/* *************************************
	 *
	 * Inputs für OneLineBindings + Methods
	 *
	 * *************************************/

	public static String[] helpInput = new String[] { "Help", "Hilfe", "/help" };

	/* GAMES getter und setter */
	public static String[] listGamesInput = new String[] { "Games", "/list_games" };
	public static String[] showGameInput = new String[] { "Show game ([0-9]+)", "/show_game_([0-9]+)" };

	/* PLAYER getter und setter */
	public static String[] listPlayersInput = new String[] { "Players", "/list_players" };
	public static String[] showProfileInput = new String[] { "/show_profile_([0-9]+)" };
	public static String[] newPlayerInput = new String[] { "/new_player (.+)" };
	public static String[] newPlayerWrongFormatInput = new String[] { "/new_player" };


	/* USER setter und getter */
	public static String[] setUsernameInput = new String[] { "/set_username_([0-9]+) (.+)" };
	public static String[] setUsernameWrongFormatInput = new String[] { "/set_username_([0-9]+)" };
	public static String[] setEmailInput = new String[] { "/set_email_([0-9]+) (.+)" };
	public static String[] setEmailWrongFormatInput = new String[] { "/set_email_([0-9]+)" };

}
