package binding.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;

import binding.Debug;
import binding.Task;
import binding.Tools;
import flunkyball.FlunkyBall;
import flunkyball.exceptions.InvalidParametersException;
import flunkyball.exceptions.ItemNotFoundException;
import flunkyball.items.Item;
import flunkyball.items.Player;
import flunkyball.items.Team;

public class AddPlayersToTeamTask extends Task {
	private int amount;

	public AddPlayersToTeamTask(FlunkyBall fb) {
		super(fb);
		this.amount = 1;
	}

	// fragt nach dem namen des nächsten spielers
	@Override
	public TaskOutput init() {
			ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
			List<KeyboardRow> keyboard = new ArrayList<>();
			for (Item item : this.getFB().getPlayerCtrl().getAllItems()) {
				Player player_keyboard = (Player) item;
				KeyboardRow row = new KeyboardRow();
				row.add(player_keyboard.getName());
				keyboard.add(row);
			}
			keyboardMarkup.setKeyboard(keyboard);

		String out_text = "Player " + this.amount + ":";
		HashMap<String, Item> out_map = new HashMap<>();
		this.setLastTaskOutput(new TaskOutput(null, out_map, null).addTextAfter(out_text));
		return this.getLastTaskOutput();
	}

	// fügt den spieler mit dem namen welcher in dem String command enthalten
	// ist zu dem team, welches in den required items ist, hinzu
	// task wird erst als completed gesetzt, wenn man einen bestimmten abbruch
	// string eingibt
	@Override
	public TaskOutput main(String command) {
		if (Tools.lowerCaseMatchesArray(command, ViewsInput.nextTask)) {
			this.setCompleted();
			String out_text = "All Players added.";
			HashMap<String, Item> out_map = new HashMap<>();
			return new TaskOutput(out_text, out_map, null);
		} else {
			if (!this.getRequiredItems().containsKey("team")) {
				throw new InvalidParametersException();
			}

			// get all attributes:
			Player player;
			try {
				player = (Player) this.getFB().getPlayerCtrl().getPlayerByName(command);
			} catch (ItemNotFoundException infe) {
				String out_text = "There is no Player with the name '" + command + "' try again!";
				return this.getLastTaskOutput().addTextAfter(out_text);
			}
			Team team = (Team) this.getRequiredItems().get("team");

			// create hashMap and item:
			HashMap<String, String> map = new HashMap<>();
			map.put("id_team", "" + team.getId());
			map.put("id_player", "" + player.getId());
			this.getFB().getTeamPlayerCtrl().createItem(map);
			this.amount++;

			ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
			List<KeyboardRow> keyboard = new ArrayList<>();
			for (Item item : this.getFB().getPlayerCtrl().getAllItems()) {
				Player player_keyboard = (Player) item;
				KeyboardRow row = new KeyboardRow();
				row.add(player_keyboard.getName());
				keyboard.add(row);
			}
			keyboardMarkup.setKeyboard(keyboard);

			// generate return
			String out_text = "New Player added\nAdd Player " + this.amount + " or go to /next_step";
			this.getLastTaskOutput().addTextBefore(team.getTeamname() + ":");
			this.getLastTaskOutput().appendLine(" - " + player.getName());
			this.getLastTaskOutput().addReplyMarkup(keyboardMarkup);
			return this.getLastTaskOutput().addTextAfter(out_text);
		}
	}
}
