package binding;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import flunkyball.FlunkyBall;

public class Main {

	public static void main(String[] args) {
		// bndCtrl.runCmd();
		ApiContextInitializer.init();
		TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
		try {
			telegramBotsApi.registerBot(new MyProjectHandler());
		} catch (TelegramApiException e) {
			BotLogger.error("LOGTAG", e);
		} // end catch()
	}

	public static class MyProjectHandler extends TelegramLongPollingBot {

		private String botUsername;

		private String botToken;

		FlunkyBall fb;
		BindingController bndCtrl;

		public MyProjectHandler() {
			String[] tokenAndUsername = readTokenAndUsernameFromFile();
			this.botToken = tokenAndUsername[0];
			this.botUsername = tokenAndUsername[1];
			this.fb = new FlunkyBall();
			this.bndCtrl = new BindingController(fb);
		}

		public static String[] readTokenAndUsernameFromFile() {
			File file_token = new File("TOKEN");
			File file_username = new File("BOTNAME");
			try {
				Scanner sc = new Scanner(file_token);
				String token = sc.nextLine();
				sc = new Scanner(file_username);
				String username = sc.nextLine();
				if (token != "" && username != "") {
					return new String[] { token, username };
				}
			} catch (FileNotFoundException fnfe) {

			}
			System.out.println("You have to create two files, TOKEN and BOTNAME.");
			System.out.println("Then you have to insert your BotToken and your BotUsername into them.");
			System.exit(1);
			return null;
		}

		@Override
		public String getBotUsername() {
			return this.botUsername;
		}

		@Override
		public String getBotToken() {
			return this.botToken;
		}

		@Override
		public void onUpdateReceived(Update update) {
			// TODO Auto-generated method stub
			if (update.hasMessage()) {
				Message message = update.getMessage();

				if (message.hasText()) {
					// get required params
					int tg_id = message.getFrom().getId();
					long chat_id = message.getChat().getId();
					String text = this.removeBotUsername(message.getText());
					System.out.println(tg_id);
					System.out.println(message.getText());

					SendMessage sendMessageRequest = this.bndCtrl.executeTelegram(tg_id, chat_id, text, message);

					try {
						sendMessage(sendMessageRequest);
					} catch (TelegramApiException e) {
						BotLogger.error("LOGTAG", e);
						// do some error handling
					}
				}
			}
		}

		private String removeBotUsername(String message) {
			if (Tools.lowerCaseMatches(message, "(.*)" + this.getBotUsername())) {
				message = Tools.getRegExGroup(message, "(.*)" + this.getBotUsername(), 1);
			}
			return message;
		}

	}
}
