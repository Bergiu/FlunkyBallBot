package binding;

import binding.Task.TaskOutput;
import binding.exceptions.BindingException;
import flunkyball.FlunkyBall;

/**
 * A OneLineBinding connects an Array of Strings as possible inputs with an
 * command that is executed if one string matches
 *
 * @author bergiu
 *
 */
public abstract class OneLineBinding implements Binding {

	private FlunkyBall fb;

	private String[] matches;

	public OneLineBinding(FlunkyBall fb, String[] matches) {
		this.fb = fb;
		this.matches = matches;
	}

	public OneLineBinding(FlunkyBall fb) {
		this.fb = fb;
	}

	protected FlunkyBall getFB() {
		return this.fb;
	}

	@Override
	public String[] getMatches() {
		return this.matches;
	}

	public boolean matches(String command) {
		if (this.matches == null) {
			return true;
		}
		for (String match : this.matches) {
			if (command.toLowerCase().matches(match.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	public abstract TaskOutput main(String command);

	@Override
	public TaskOutput execute(String command) {
		if (this.matches(command)) {
			return this.main(command);
		} else {
			throw new BindingException();
		}
	}
}
