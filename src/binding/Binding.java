package binding;

import binding.Task.TaskOutput;

public interface Binding {

	public TaskOutput execute(String command);

	public String[] getMatches();

	public boolean matches(String command);
}
