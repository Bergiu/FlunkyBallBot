package binding;

import binding.Task.TaskOutput;
import binding.exceptions.AllTasksCompletedException;
import binding.exceptions.BindingException;
import flunkyball.FlunkyBall;

public abstract class MultiLineBinding extends OneLineBinding {

	private boolean started;
	private boolean completed;

	public MultiLineBinding(FlunkyBall fb, String[] matches) {
		super(fb, matches);
		this.started = false;
		this.completed = false;
	}
	
	public MultiLineBinding(MultiLineBinding mlb){
		//getFB muss nicht gekloned werden, da es immer gleich ist
		//getMatches muss auch nicht gekloned werden, weil es nur ein string array ist
		super(mlb.getFB(), mlb.getMatches());
		this.started = mlb.started;
		this.completed = mlb.completed;
	}

	public boolean isCompleted() {
		return this.completed;
	}

	public void setCompleted() {
		this.completed = true;
	}

	// in der init methode werden jetzt dem MultiLineBinding die tasks
	// hinzugefügt und sonstige sachen ausgeführt, welche vorher gemacht werden
	public abstract TaskOutput init(String command);

	// in der main werden jetzt die tasks ausgeführt und zum schluss wird this
	// auf completed gesetzt
	// public abstract TaskOutput main();

	public abstract TaskOutput exit(TaskOutput out);
	
	public abstract TaskOutput cancel();

	public abstract MultiLineBinding clone();

	@Override
	public boolean matches(String command) {
		if ((this.getMatches() == null) || this.started || this.completed) {
			return true;
		}
		for (String match : this.getMatches()) {
			if (command.toLowerCase().matches(match.toLowerCase())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public TaskOutput execute(String command) {
		// 1. wenn keine matches da sind, führe direkt aus
		// 2. wenn this ist not started, execute init(), falls ein match passt
		// 3. wenn this ist started, dann execute main(), ohne überprüfung von
		// den matches, weil es wirde ja bereits gestarted
		// 4. execute main() nur, wenn noch nicht completed
		// in main() wird dann gesetzt, wann dieser multilineBinding completed
		// ist
		// 5. wenn alle die main sagt, dass dieses multilineBinding completed
		// ist, dann wird nochmal die exit methode ausgeführt


		if (this.matches(command)) {
			if (!this.started) {
				this.started = true;
				return this.init(command);
			} else {
				if (!this.completed) {
					TaskOutput out = this.main(command);
					if (this.isCompleted()) {
						return this.exit(out);
					} else {
						return out;
					}
				} else {
					throw new AllTasksCompletedException();
				}
			}
		} else {
			throw new BindingException();
		}
	}

}
