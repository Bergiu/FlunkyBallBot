package binding;

import java.util.HashMap;

import org.telegram.telegrambots.api.objects.replykeyboard.ForceReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;

import binding.exceptions.TaskCompletedException;
import flunkyball.FlunkyBall;
import flunkyball.items.Item;

/**
 * A Task is a Binding that can be completed
 *
 * @author bergiu
 *
 */
public abstract class Task {

	private FlunkyBall fb;
	private HashMap<String, Item> requiredItems;

	private boolean started;
	private boolean completed;

	private TaskOutput lastTaskOutput;

	public Task(FlunkyBall fb) {
		this.fb = fb;
		this.started = false;
		this.completed = false;
		this.requiredItems = new HashMap<>();
	}

	public FlunkyBall getFB() {
		return this.fb;
	}

	public void setRequiredItems(HashMap<String, Item> requiredItems) {
		this.requiredItems = requiredItems;
	}

	public HashMap<String, Item> getRequiredItems() {
		return this.requiredItems;
	}

	public boolean isStarted() {
		return this.started;
	}

	public boolean isCompleted() {
		return this.completed;
	}

	public void setCompleted() {
		this.completed = true;
	}

	// init befehl, kann einen befehl ausführen und ein output erzeugen
	// ist für sowas gedacht, wie: "Enter Team name from team $id: "
	// dabei muss man dann vorher einen befehl ausführen, welcher die anzahl der
	// teams zählt um die id zu bekommen
	public abstract TaskOutput init();

	// throws an exception if input is invalid
	// setsCompeted if it is completed
	// enthält die eigentliche logik eines tasks
	// hier wird z.b. ein Team erstellt, mit dem im String command enthaltenen
	// namen des teams und dem in den requiredItems enthaltenen game
	public abstract TaskOutput main(String command);

	// returnt zuerst, wenn der task noch nicht gestartet wurde die getMessage
	// und falls doch die methode command
	public TaskOutput execute(String command) {
		if (!this.isStarted()) {
			TaskOutput out = this.init();
			this.started = true;
			// TODO: init sollte started selber setzen
			return out;
		} else if (!this.isCompleted()) {
			TaskOutput out = this.main(command);
			return out;
		} else {
			throw new TaskCompletedException();
		}
	}

	public TaskOutput getLastTaskOutput() {
		return this.lastTaskOutput;
	}

	public void setLastTaskOutput(TaskOutput lastTaskOutput) {
		this.lastTaskOutput = lastTaskOutput;
	}

	// TODO: not static inner class
	public static class TaskOutput {

		// if an error message is appended more than one time the original text
		// can be combined with it
		private String before;

		private String after;

		// the output string that should be displayed in the chat
		private String text;

		// the keyboard that is displayed with the output string
		public ReplyKeyboard replyMarkup;

		// the generated items to give it to the next task
		public HashMap<String, Item> generatedItems;

		public TaskOutput(String text, HashMap<String, Item> generatedItems, ReplyKeyboard replyMarkup) {
			this.text = text;
			this.generatedItems = generatedItems;
			this.replyMarkup = replyMarkup;
		}

		public String getText() {
			String out = "";
			if (before != null) {
				out += before + "\n";
			}
			// zwischen before und text eine zeile
			if (before != null && text != null) {
				out += "\n";
			}
			if (text != null) {
				out += text + "\n";
			}
			// zwischen text und after eine zeile, und wenn es keinen text gibt,
			// dann zwischen before und after
			if ((after != null && text != null) || (text == null && after != null && before != null)) {
				out += "\n";
			}
			if (after != null) {
				out += after;
			}
			return out;
		}

		public void setText(String text) {
			this.text = text;
		}

		/**
		 * Temporarily adds a new line to the top of the old text
		 * 
		 * @param text
		 * @return
		 */
		public TaskOutput addTextBefore(String text) {
			this.before = text;
			return this;
		}

		/**
		 * Temporarily adds a new line to the bottom of the old text
		 * 
		 * @param text
		 * @return
		 */
		public TaskOutput addTextAfter(String text) {
			this.after = text;
			return this;
		}

		/**
		 * Permanently adds a new line to the bottom of the old text
		 * 
		 * @param text
		 * @return
		 */
		public TaskOutput appendLine(String text) {
			if (this.text == null) {
				this.setText(text);
			} else {
				this.setText(this.text + "\n" + text);
			}
			return this;
		}

		public TaskOutput addGeneratedItems(HashMap<String, Item> generatedItems) {
			if (this.generatedItems != null) {
				if (generatedItems != null) {
					this.generatedItems.putAll(generatedItems);
				}
			} else {
				this.generatedItems = generatedItems;
			}
			return this;
		}

		public TaskOutput addReplyMarkup(ReplyKeyboard replyMarkup) {
			if (replyMarkup != null) {
				this.replyMarkup = replyMarkup;
			}
			return this;
		}
	}

}