package binding.exceptions;

/**
 * Is thrown, when a binding does not match
 *
 * @author bergiu
 *
 */
public class BindingException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
