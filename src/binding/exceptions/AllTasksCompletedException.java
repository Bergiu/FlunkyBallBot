package binding.exceptions;

/**
 * Is thrown when a all Tasks from a MultiLineBinding are comleted
 *
 * @author bergiu
 *
 */
public class AllTasksCompletedException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

}
