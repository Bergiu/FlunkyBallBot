package binding.exceptions;

public class ViewException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ViewException() {
		super();
	}

	public ViewException(String s) {
		super(s);
	}
}
