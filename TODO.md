# TODO

##Tasks
- [x] Grundfunktionalität Items (9/10)
- [x] Grundfunktionalität Controller (6/7)
- [x] database (4/5)
- [x] MasterController (FlunkyBall class)
- [x] grobe tests
- [x] Klasse um commands an functions zubinden
- [ ] views
- [x] history per user (gelöst durch multiLineBinding)
- [ ] log
- [ ] secondary keys adds automatically to items (z.b. ctrl.updateForeignKeys())
- [ ] test
- [x] Telegramverbindung
- [ ] Permissions
- [ ] performance (do not load all items and real database)
	- [ ] create tables for something that is used recently like friends with friendscore

### ideas:
- [x] load token and botname from file, and add file to gitignore
- [ ] user status
- [ ] rename attribute place to location in Game
- [ ] addPlayersToTeamTask should list team name and already added players
- [ ] create a new player in addPlayersToTeamTask
- [x] TaskOutput.out change to text
- [ ] new team name keyboard with random names
- [ ] 2 branches erstellen: master, dev -> beide automatisch nach einem commit compilieren und ausführen auf 2 unterschiedlichen bots, @flunkyball_stats_bot und @flunkyball_stats_dev_bot

### BUGS
- [ ] on creating a new game the same player can be added more than one time in a game
	- [ ] game.getAllPlayers
- [ ] on creating a team the team can be empty
	- [ ] abbruch message erst abfragen, wenn bereits spieler srinne sind
- [ ] cancel multiline binding
	- [ ] bei new game, wenn man bei den teams erstellen abbricht, ist ein game da, dass keine teams hat. und da man die teams nicht nachträglich hinzufügen kann ist das schon ziemlich nutzlos ...
- [ ] automatisch reply to message in chats
- [x] jemand anderes kann meine multilinebinding bearbeiten
- [x] wenn ich ein multilinebinding in einer gruppe starte, bin ich in einem anderen chat immernoch dadrinne
- [ ] ein neuer nutzer wird automatisch angelegt, es sollte eine on registration message geben, wo z.b. drinne steht: "wähle deinen player und setze deinen username"

### Views
- [ ] Profile(Player)
	- [ ] set_username
	- [ ] select_player
		- [ ] sollte eine liste mit playern geben, eine suche enthalten und die player beschreiben (die spiele, die sie bereits gespielt haben)
- [ ] Games
	- [x] list all
	- [ ] list my games
	- [ ] list games from player
	- [x] show
	- [x] create
		- [x] dabei create new team
		- [ ] create new user if not found
		- [ ] on /cancel delete all created
		- [ ] set location and place for teams
	- [ ] change Attributes
	- [ ] delete
- [ ] Players with Users
	- [ ] list
		- [x] all (reihenfolge egal)
		- [ ] all (alphabetisch)
		- [ ] top
		- [ ] friends (die mit denen man schonmal ein spiel hatte)
			- [ ] player controller getFriends(Player player) -- muss die aber auch am besten sortiert zurückgeben, also die mit den meisten gemeinsamen spielen zuerst
	- [ ] show
		- [ ] show stats like games played /show_games_from_user$id or total score
		- [ ] My Profile
	- [ ] create (new player)
	- [ ] change Attributes
	- [ ] delete

### Test
- [x] creating new game

### MasterController
- [x] create all controllers
- [x] initialize all data

### Controller:
- [x] Interface
- [x] Superclass
- [x] getById/getAll
	- [ ] check if item is in db, if not found
- [x] saveAllToDB
- [x] loadAllFromDB
- [x] createItem
- [ ] remove items
- [x] makeItem
	- [x] Game
	- [x] Player
	- [x] Team
	- [x] TeamPlayer
	- [x] User

### DB
- [x] read file
- [x] create file
- [x] save file
- [x] database (textfile) connection
- [x] remove ; on save
- [x] update items
- [x] insert items
- [ ] remove items

### Items
- [x] Interface
- [x] Attribute, Getter, Setter
	- [x] Game
	- [x] Player
	- [x] Team
	- [x] TeamPlayer
	- [x] User
- [x] Constructors
	- [x] mit hashmap
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] toHashMap
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] Fremdschlüssel objekte als attribute
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] Fremdschlüssel objekte im construktor
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] Fremdschlüssel objekte getter setter
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] Fremdschlüssel objekte getter setter und constuktor werfen fehler wenn objekt nicht gefunden wird
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [x] setter in constuctor benutzen
		- [x] Game
		- [x] Player
		- [x] Team
		- [x] TeamPlayer
		- [x] User
- [ ] values in setter überprüfen und error werfen
		- [ ] Game
		- [ ] Player
		- [ ] Team
		- [ ] TeamPlayer
		- [ ] User
